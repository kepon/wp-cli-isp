#!/bin/bash

# Lancer le prompt wp-cli avec le bon user sous ispconfig
# By David Mercereau
# Licence Beerware

WP_CLI_BIN="/opt/wp-cli-isp/wp-cli.phar"
PHP_CLI_BIN="/usr/bin/php8.2"

! [ -f ${WP_CLI_BIN} ] && echo "${WP_CLI_BIN} not exist install wp-cli : https://wp-cli.org/"

if ! [ -f 'wp-login.php' ]; then
    echo "Wordpress dosn't exist hear"
fi

siteId=`stat -c '%U' .`

sudo -u ${siteId} ${PHP_CLI_BIN} ${WP_CLI_BIN} $@

exit


