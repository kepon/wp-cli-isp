# WP CLI ISP - wp cli bash script for ispconfig

## Not support

Replace by : https://framagit.org/kepon/isp-php-scanner/-/blob/main/plugins.d/WpUpdate.md & https://framagit.org/kepon/isp-php-scanner/-/blob/main/plugins.d/WpCacheDeploy.md

Ispconfig panel multiple WP manage with wp cli https://wp-cli.org/

- Update wordpress

- Install and configure w3 super cache if not installed

Autor : David Mercereau

Licence Beerware

Installation : 

```bash
cd /opt
# Download wp-manage.sh
git clone https://framagit.org/kepon/wp-cli-isp.git
cd wp-cli-isp
# Install wp cli
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
```

Change parameter in script header : 

```bash
EXCLUDE_UPDATE_FILE="/opt/wp-cli-isp/update.exclude"
EXCLUDE_CACHE_FILE="/opt/wp-cli-isp/cache.exclude"
WP_CLI_BIN="/opt/wp-cli-isp/wp-cli.phar"
PHP_CLI_BIN="/usr/bin/php"
W3_TOTAL_CACHE_CONF="/opt/wp-cli-isp/w3-total-cache.json"
```

Launch first (confirme) : 

```
/opt/wp-cli-isp/wp-cli-isp.sh  -v -c
```

Verbose

```
/opt/wp-cli-isp/wp-cli-isp.sh  -v
```

Silence

```
/opt/wp-cli-isp/wp-cli-isp.sh  
```

Exclude update : add line on /opt/wp-cli-isp/update.exclude

Exclude cache install : add line on /opt/wp-cli-isp/cache.exclude

