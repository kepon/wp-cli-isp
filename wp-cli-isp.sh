#!/bin/bash

# Ispconfig multiple WP manage with wp cli 
# 	- Update wordpress
# 	- Install and configure w3 super cache if not installed
#
# By David Mercereau
# Licence Beerware

DEBUG=1
CHECK=0
EXCLUDE_UPDATE_FILE="/opt/wp-cli-isp/update.exclude"
EXCLUDE_CACHE_FILE="/opt/wp-cli-isp/cache.exclude"
EXCLUDE_PATH_FILE="/opt/wp-cli-isp/path.exclude" 
WP_CLI_BIN="/opt/wp-cli-isp/wp-cli.phar"
WP_CLI_ARGS=" --skip-packages --skip-plugins  --skip-themes "
PHP_CLI_BIN="/usr/bin/php8.2"
TIMEOUT_BIN="/usr/bin/timeout"
W3_TOTAL_CACHE_CONF="/opt/wp-cli-isp/w3-total-cache.json"
ISPCONFIG_API="/opt/wp-cli-isp/ispconfig.api.get"

while getopts "h?cd:vd:" opt; do
    case "$opt" in
    h|\?)
        echo "Use $0 (-v)"
        exit 0
        ;;
    v)  DEBUG=1
        ;;
    c)  CHECK=1
        ;;
    esac
done

! [ -f ${WP_CLI_BIN} ] && echo "${WP_CLI_BIN} not exist install wp-cli : https://wp-cli.org/"

find /var/www/clients -name wp-login.php | while IFS=$'\n' read f ; do
    clientId=`echo $f | cut -d"/" -f5`
    siteId=`echo $f | cut -d"/" -f6`
    siteDir=`dirname $f`
    
    ### Exclude path
    grep ${f} ${EXCLUDE_PATH_FILE} &>/dev/null
    if ! (($?)) ; then
	[ ${DEBUG} == 1 ] && echo "${f} exclude path (${EXCLUDE_PATH_FILE})"
	continue
    fi
    
    WP_CLI_CACHE_DIR="/var/www/clients/${clientId}/${siteId}/tmp/.wp-cli/"
    export WP_CLI_CACHE_DIR
    siteName=`ls /var/www/ -l | grep "${clientId}/${siteId}" | awk '{print $9}'`
    siteUrl=`su ${siteId} -s /bin/bash -c "${TIMEOUT_BIN} 50 ${PHP_CLI_BIN} ${WP_CLI_BIN} option get siteurl --path='${siteDir}' ${WP_CLI_ARGS}"  2>/dev/null`
    if (($?)); then
	echo "${siteName} probably timeout"
	echo "su ${siteId} -s /bin/bash -c \"${TIMEOUT_BIN} 50${PHP_CLI_BIN} ${WP_CLI_BIN} option get siteurl --path='${siteDir}' ${WP_CLI_ARGS}\""
	continue
    fi
    siteLibelle=${siteName}

    [ ${DEBUG} == 1 ] && echo "# ${siteLibelle} (${clientId}/${siteId})"

    ls /etc/apache2/sites-enabled/*${siteName}.vhost &>/dev/null
    if (($?)) ; then
    	[ ${DEBUG} == 1 ] && echo "This site is probably disable"
    	continue
    fi

    if [ ${CHECK} == 1 ]; then
	read -p "Continue ? (Y/n/s = skip this) " -n 1 -r </dev/tty
	echo
	if [[ $REPLY =~ ^[Ss]$ ]]
	then
		echo "Skip this"
	continue
	elif [[ ! $REPLY =~ ^[Yy]$ ]]
	then
		echo "Exit by user..."
	exit 0
	fi
    fi

    ### W3 total cache
    grep ${siteLibelle} ${EXCLUDE_CACHE_FILE} &>/dev/null
    if (($?)) ; then
	    # Test if  w3-total-cache is hear
	    su ${siteId} -s /bin/bash -c "${PHP_CLI_BIN} ${WP_CLI_BIN} plugin list --path='${siteDir}' ${WP_CLI_ARGS}"  | grep "w3-total-cache" &>/dev/null
	    # If not present
	    if (($?)) ; then
	    	[ ${DEBUG} == 1 ] && echo "w3-total-cache not present, installation"
	    	# Installation w3 total cache
	    	su ${siteId} -s /bin/bash -c "${PHP_CLI_BIN} ${WP_CLI_BIN} --quiet --path='${siteDir}' ${WP_CLI_ARGS} plugin install w3-total-cache "  >/dev/null
	    	if (($?)); then
			echo "${siteLibelle} w3-total-cache install error"
	    	else 
		    # Activate
		    su ${siteId} -s /bin/bash -c "${PHP_CLI_BIN} ${WP_CLI_BIN} --quiet --path='${siteDir}'${WP_CLI_ARGS} plugin activate w3-total-cache"  >/dev/null
		    # Import config file
		    su ${siteId} -s /bin/bash -c "${PHP_CLI_BIN} ${WP_CLI_BIN} --quiet --path='${siteDir}'  w3-total-cache import ${W3_TOTAL_CACHE_CONF} "  >/dev/null
		    if (($?)); then
			    echo "${siteLibelle} w3-total-cache import error"
		    fi
		fi
	    fi
	else
		[ ${DEBUG} == 1 ] && echo "${siteLibelle} cache exclude (${EXCLUDE_CACHE_FILE})"
	fi

    ### Auto update
    grep ${siteLibelle} ${EXCLUDE_UPDATE_FILE} &>/dev/null
    if (($?)) ; then
	# core
	su ${siteId} -s /bin/bash -c "${PHP_CLI_BIN} ${WP_CLI_BIN} --path='${siteDir}' core update ${WP_CLI_ARGS}"  >/dev/null
	if (($?)) ; then
	    echo "${siteLibelle} core update error"
	fi
	# plugin
	su ${siteId} -s /bin/bash -c "${PHP_CLI_BIN} ${WP_CLI_BIN} --path='${siteDir}' plugin update --all ${WP_CLI_ARGS}"  >/dev/null
	if (($?)) ; then
	    echo "${siteLibelle} plugin update error"
	fi
	# theme
	su ${siteId} -s /bin/bash -c "${PHP_CLI_BIN} ${WP_CLI_BIN} --path='${siteDir}' theme update --all ${WP_CLI_ARGS}"  >/dev/null
	if (($?)) ; then
	    echo "${siteLibelle} theme update error"
	fi
    else
	[ ${DEBUG} == 1 ] && echo "${siteLibelle} update exclude (${EXCLUDE_UPDATE_FILE})"
    fi
	
done

